from werkzeug.wrappers import request
import psycopg2
from environs import Env

env = Env()
env.read_env()


def conn_cur():
    conn = psycopg2.connect(
        host=env("host"),
        database=env("database"),
        user=env("user"),
        password=env("password"),
    )

    cur = conn.cursor()

    return (
        conn,
        cur,
    )




class TabelaSeries:
    campos_da_tabela = ["id", "serie", "seasons", "released_date", "genre", "imdb_rating"]

    def _criar_tabela(self) -> None:
        conn, cur = conn_cur()

        cur.execute(
            """
                CREATE TABLE IF NOT EXISTS ka_series
                    (
                        id BIGSERIAL PRIMARY KEY,
                        serie VARCHAR(100) NOT NULL UNIQUE,
                        seasons INTEGER NOT NULL,
                        released_date DATE NOT NULL,
                        genre VARCHAR(50) NOT NULL,
                        imdb_rating FLOAT NOT NULL
                    );
            """
        )

        conn.commit()
        cur.close()
        conn.close()

    def criar_estado(self, data: dict):
        conn, cur = conn_cur()
        self._criar_tabela()
        cur.execute(
            """
                INSERT INTO ka_series
                    (serie, seasons, released_date, genre, imdb_rating)
                VALUES
                    (%(serie)s, %(seasons)s, %(released_date)s, %(genre)s, %(imdb_rating)s)
                RETURNING *
            """,
            data,
        )

        query = cur.fetchone()

        conn.commit()
        cur.close()
        conn.close()

        
        resposta = dict(zip(self.campos_da_tabela, query))
        

        return resposta
    
    def pegar_dados(self):
        try:
            conn, cur = conn_cur()
            cur.execute("SELECT * FROM ka_series")
            query = cur.fetchall()
            conn.commit()
            cur.close()
            conn.close()
            resposta = [dict(zip(self.campos_da_tabela, retorno)) for retorno in query]
            return resposta
        except:
            return [] 


def     selecionar_serie(serie_id: int) -> dict:
    try:
        series = TabelaSeries()
        conn, cur = conn_cur()
        series._criar_tabela()

        cur.execute(
            """
                    SELECT *
                    FROM
                        ka_series e
                    WHERE
                        e.id = '%s'
                """
            % serie_id,
        )

        conn.commit()
        query = cur.fetchone()
        cur.close()
        conn.close()
        resposta = dict(zip(series.campos_da_tabela, query))
        return resposta
    except:
        return {}

