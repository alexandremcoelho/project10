from flask import Flask, request, jsonify
from .services import TabelaSeries, selecionar_serie


app = Flask(__name__)


@app.route("/series", methods=["GET", "POST"])
def create():
    series = TabelaSeries()
    if request.method == "POST":
        data = request.get_json()
        retorno = series.criar_estado(data)
        retorno["released_date"] = data["released_date"]

        return retorno, 201
    
    output = jsonify({"data": series.pegar_dados()})
    
    return output, 200
    


    


@app.route("/series/<int:serie_id>")
def select_by_id(serie_id):
    if(selecionar_serie(serie_id)):
        output = jsonify({"data": selecionar_serie(serie_id)})
        return output
    return {},404


